import Vue from 'vue';
import Vuex from 'vuex';
const withQuery = require('with-query').default;

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    root: {
      products: [],
      isFetchProductsPending: false,
      fetchProductsError: null
    }
  },
  mutations: {
    fetchProductsPending(state) {
      state.root.isFetchProductsPending = true;
    },
    fetchProductsSuccess(state, response) {
      state.root.isFetchProductsPending = false;
      state.root.products = response;
      state.root.fetchProductsError = null;
    },
    fetchProductsError(state, error) {
      state.root.isFetchProductsPending = false;
      state.root.products = [];
      state.root.fetchProductsError = error;
    }
  },
  actions: {
    fetchProducts(url, query){
      return (context) => {
        context.commit('fetchProductsPending');

        fetch(withQuery(url, query), {
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'GET'
        })
        .then(response => response.json())
        .then(response => {
          context.commit('fetchProductsSuccess', response);
          //searchResultsRef.current.scrollIntoView({behavior: 'smooth'});
        })
        .catch(error => {
          context.commit('fetchProductsError', error);
          //toastr.error('An error occured', 'No results found', {className: 'error-toastr'});
        });
      }
    }
  }
});
