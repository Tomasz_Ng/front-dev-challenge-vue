import {
  StyledErrorPageContainer
} from '../../theme/styles';

export default {
  render () {
    return (
      <StyledErrorPageContainer>
        <h1>404 error page</h1>
        <p>Oops, the page you requested doesn't exist.</p>
      </StyledErrorPageContainer>
    );
  }
};
