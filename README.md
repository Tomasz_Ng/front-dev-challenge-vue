### Front dev challenge Vue

#### Commands
- yarn serve (Run development environment)
- yarn build (Build for deployment)
- yarn test (Run unit tests)

#### Features

- Search
- Pagination
- Toastr notifications
- Error page handling
- Unit tests

#### Browser support

- Chrome
- Safari
- Edge
- Firefox
- Opera
