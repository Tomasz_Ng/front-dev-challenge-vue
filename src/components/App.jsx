export default {
  render() {
    return (
      <main>
        <router-view></router-view>
      </main>
    )
  }
};
