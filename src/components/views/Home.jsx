import {mapState, mapActions} from 'vuex';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import {
  StyledPageHead,
  StyledPageDescription,
  StyledPageTitle,
  StyledPageImage,
  StyledPageContent,
  StyledSearchContainer,
  StyledSearchForm,
  StyledSearchInput,
  StyledSearchSubmit,
  StyledSearchResult,
  StyledSearchProduct,
  StyledTotalResults
} from '../../theme/styles';

export default {
  computed: {
    ...mapState({
      isFetchProductsPending: state => state.root.isFetchProductsPending,
      products: state => state.root.products,
      fetchProductsError: state => state.root.fetchProductsError
    })
  },
  methods: {
    ...mapActions([
      'fetchProducts'
    ]),
    handleSearchSubmit: async (event) => {
      event.preventDefault();

      this.fetchProducts(process.env.API_URL + '/product', {
        q: event.target[0].value,
        limit: 10,
        page: 1
      })
    }
  },
  render () {
    return (
      <div>
        <StyledPageHead>
          <StyledPageDescription>
            <StyledPageTitle>This is a page for <br/> beauty product search</StyledPageTitle>
          </StyledPageDescription>

          <StyledPageImage className="page-image"></StyledPageImage>
        </StyledPageHead>

        <StyledPageContent>
          <StyledSearchContainer>
          <StyledSearchForm onSubmit={this.handleSearchSubmit} data-testid="form">
          <StyledSearchInput type="search" placeholder="Enter a product name or an ingredient..." aria-label="search"/>
          <StyledSearchSubmit type="submit" value="Search"/>
        </StyledSearchForm>

        {this.isFetchProductsPending ?
          <PulseLoader color="#55D7FF"/> :
          this.fetchProductsError !== null ?
            <StyledTotalResults><strong>Total results :</strong> {this.products.length}</StyledTotalResults> :
          this.products.length > 0 ?
            <StyledSearchResult>
              {this.products.map((product, index) => {
                return <StyledSearchProduct key={index}>
                  <p><strong>{product.brand.charAt(0).toUpperCase() + product.brand.slice(1)}</strong> - <span>{product.name.charAt(0).toUpperCase() + product.name.slice(1)}</span></p>
                </StyledSearchProduct>
              })}
            </StyledSearchResult> : null}
          </StyledSearchContainer>
        </StyledPageContent>
      </div>
    )
  }
};
