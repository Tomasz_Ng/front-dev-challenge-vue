import Home from '../components/views/Home';
import Error from '../components/views/Error';

export default [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  { path: '*',
    name: 'error',
    component: Error
  }
];
